var express=require('express');
var app=express();
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.get('/', function (req, res) {
    //res.send("<html><body><h1>Hello World</h1></body></html>");
    res.sendFile(__dirname+'/index.html')
});
 //以物件res回傳一個靜態文件=index.html
 //加了 __dirname＝node.js關鍵字，用來返回目前這個.js檔的絕對路徑，後面附上 /檔名
app.post('/submit-data', function (req, res) {
    var name = req.body.firstName + ' ' + req.body.lastName;
    res.send(name + ' Submitted Successfully!');
});
 
app.put('/update-data', function (req, res) {
    res.send('PUT Request');
});
 
app.delete('/delete-data', function (req, res) {
    res.send('DELETE Request');
});
var server=app.listen(3000,function(){
    console.log('node server is running');
})