# NodejsPracticeForExpress


**Express 簡介**
- 是基於node.js web application的一種開發框架，可以提供使用者快速搭建一個網站
- 屬於中介軟題模組(connect)
- 主要在處理http請求函數
- 包含不同中介函數，可以在req和res間，加入一些任務執行
- 允許定義錯誤處理
- 允許建立REST API Server
- 易於和db連線

**安裝指南**

安裝成全域，只需在cmd 輸入以下指令 : npm install -g express
安裝在指定專案
1. mkdir 資料夾名稱(若已經創建則不用此步驟)
2. cd 專案資料夾
3. cmd prompt npm init
4. npm install express --save
可以在package.json檔裡面的dependencies看見裝好的express

Start

簡單的EXpress 範例
```
var express = require('express');
var app = express();
 
// define routes here..
 
var server = app.listen(300, function () {
    console.log('Node server is running..');
//app.listen== http.Server.listen() 設立主機位置和port
});
```
這步驟還尚未定義任何route，因此還無法顯示任何訊息

**設定Route**
```
var express = require('express');
var app = express();

app.get('/', function (req, res) {
    res.send("<html><body><h1>Hello World</h1></body></html>");
});
 
app.post('/submit-data', function (req, res) {
    res.send('POST Request');
});
 
app.put('/update-data', function (req, res) {
    res.send('PUT Request');
});
 
app.delete('/delete-data', function (req, res) {
    res.send('DELETE Request');
});
 
var server = app.listen(5000, function () {
    console.log('Node server is running..');
});
```
利用物件app建立了四個route，其中在index的時候會回傳hello world 
接著只要修改路由裡面的code就可以指向不同的頁面
